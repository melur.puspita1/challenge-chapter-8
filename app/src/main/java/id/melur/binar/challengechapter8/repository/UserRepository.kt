package id.melur.binar.challengechapter8.repository

import android.content.Context
import id.melur.binar.challengechapter8.model.LocalDatabase
import id.melur.binar.challengechapter8.model.user.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(context: Context) {

    private val mDb = LocalDatabase.getInstance(context)

    suspend fun getUserByEmail(email: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUserByEmail(email)
    }

    suspend fun getUsernameByEmail(email: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUsernameByEmail(email)
    }

    suspend fun insert(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.insertUser(user)
    }

}