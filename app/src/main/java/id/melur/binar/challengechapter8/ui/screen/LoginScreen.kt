package id.melur.binar.challengechapter8.ui.screen

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import id.melur.binar.challengechapter8.viewmodel.LoginViewModel

@Composable
fun LoginScreen(navController: NavHostController, loginViewModel: LoginViewModel) {

    val context = LocalContext.current

    Column(
        horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier
            .fillMaxSize()
    ) {

        Text(
            text = "Login",
            fontSize = MaterialTheme.typography.h4.fontSize,
            modifier = Modifier.padding(top = 48.dp)
        )

        Image(
            painter = painterResource(id = id.melur.binar.challengechapter8.R.drawable.ic_launcher_background),
            contentDescription = "",
            modifier = Modifier
                .width(188.dp)
                .height(141.dp)
                .padding(top = 32.dp)
        )


        OutlinedTextField(
            value = loginViewModel.email,
            onValueChange = { loginViewModel.emailTextField(it) },
            label = { Text(text = "Masukkan Email") },
            singleLine = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 40.dp)
        )

        OutlinedTextField(
            value = loginViewModel.password,
            onValueChange = { loginViewModel.passwordTextField(it) },
            singleLine = true,
            label = { Text(text = "Masukkan Password") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            visualTransformation = if (loginViewModel.passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image =
                    if (loginViewModel.passwordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
                val description =
                    if (loginViewModel.passwordVisible) "Hide Password" else "Show Password"

                IconButton(onClick = {
                    loginViewModel.passwordVisible = !loginViewModel.passwordVisible
                }) {
                    Icon(imageVector = image, description)
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 16.dp)
        )

        Button(
            onClick = {
                loginViewModel.checkRegistered()
                loginViewModel.checkLogin(loginViewModel.email)
                if (!loginViewModel.notregistered && loginViewModel.login) {
                    loginViewModel.saveDataStore(loginViewModel.email)
                    navController.navigate("home") {
                        popUpTo("login") {
                            inclusive = true
                        }
                    }
                    loginViewModel.email = ""
                    loginViewModel.password = ""
                } else {
                    Toast.makeText(context, "Email atau Password tidak sesuai", Toast.LENGTH_SHORT)
                        .show()
                }
            }, modifier = Modifier
                .fillMaxWidth()
                .padding(top = 150.dp, start = 16.dp, end = 16.dp),
            enabled = loginViewModel.email.isNotBlank() && loginViewModel.password.isNotBlank()
        ) {
            Text(text = "Login")
        }

        Text(text = "Belum punya akun?", modifier = Modifier
            .padding(top = 8.dp)
            .clickable {
                navController.navigate("register")
                loginViewModel.email = ""
                loginViewModel.password = ""
            })
    }
}

@Preview(showBackground = true)
@Composable
fun LoginScreenPreview() {
    val viewModel = hiltViewModel<LoginViewModel>()
    LoginScreen(navController = rememberNavController(), viewModel)
}