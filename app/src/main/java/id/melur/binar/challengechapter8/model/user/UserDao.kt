package id.melur.binar.challengechapter8.model.user

import androidx.room.*

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getUser(): List<User>

    @Query("SELECT * FROM User WHERE email = :query")
    fun getUserByEmail(query: String): User

    @Query("SELECT username FROM User WHERE email = :query")
    fun getUsernameByEmail(query: String): String

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User): Int

    @Delete
    fun deleteUser(user: User): Int
}