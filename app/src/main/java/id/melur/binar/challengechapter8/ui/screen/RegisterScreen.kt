package id.melur.binar.challengechapter8.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import id.melur.binar.challengechapter8.model.user.User
import id.melur.binar.challengechapter8.viewmodel.RegisterViewModel

@Composable
fun RegisterScreen(navController: NavHostController, registerViewModel: RegisterViewModel) {
    registerViewModel.checkRegistered()

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {

        Text(
            text = "Register",
            fontSize = MaterialTheme.typography.h4.fontSize,
            modifier = Modifier
                .padding(top = 48.dp)
        )

        Image(
            painter = painterResource(id = id.melur.binar.challengechapter8.R.drawable.ic_launcher_background),
            contentDescription = "",
            modifier = Modifier
                .width(188.dp)
                .height(141.dp)
                .padding(top = 32.dp)
        )

        OutlinedTextField(
            value = registerViewModel.username,
            onValueChange = { registerViewModel.usernameTextField(it) },
            singleLine = true,
            label = { Text(text = "Masukkan Username") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 40.dp)
        )

        OutlinedTextField(
            value = registerViewModel.email,
            onValueChange = { registerViewModel.emailTextField(it) },
            singleLine = true,
            label = { Text(text = "Masukkan Email") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 16.dp)
        )

        OutlinedTextField(
            value = registerViewModel.password,
            onValueChange = { registerViewModel.passwordTextField(it) },
            label = { Text(text = "Masukkan Password") },
            singleLine = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 16.dp),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            visualTransformation = if (registerViewModel.passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image =
                    if (registerViewModel.passwordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
                val description =
                    if (registerViewModel.passwordVisible) "Hide Password" else "Show Password"

                IconButton(onClick = {
                    registerViewModel.passwordVisible = !registerViewModel.passwordVisible
                }) {
                    Icon(imageVector = image, contentDescription = description)
                }
            }
        )

        OutlinedTextField(
            value = registerViewModel.confirm,
            onValueChange = { registerViewModel.confirmTextField(it) },
            label = { Text(text = "Masukkan Konfirmasi Password") },
            singleLine = true,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 16.dp),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            visualTransformation = if (registerViewModel.confPasswordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            trailingIcon = {
                val image =
                    if (registerViewModel.confPasswordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
                val description =
                    if (registerViewModel.confPasswordVisible) "Hide Password" else "Show Password"

                IconButton(onClick = {
                    registerViewModel.confPasswordVisible = !registerViewModel.confPasswordVisible
                }) {
                    Icon(imageVector = image, contentDescription = description)
                }
            }
        )


        Button(
            onClick = {
                val user = User(
                    null,
                    registerViewModel.username,
                    registerViewModel.email,
                    registerViewModel.password
                )
                registerViewModel.insertUser(user)
                navController.navigate("login") {
                    popUpTo("register") {
                        inclusive = true
                    }
                }
                registerViewModel.username = ""
                registerViewModel.email = ""
                registerViewModel.password = ""
                registerViewModel.confirm = ""
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 48.dp),
            enabled = registerViewModel.username.isNotBlank() && registerViewModel.email.isNotBlank() && registerViewModel.password.isNotBlank() && registerViewModel.confirm == registerViewModel.password && registerViewModel.notregistered
        ) {

            Text(text = "Register")
        }

    }


}

@Preview(showBackground = true)
@Composable
fun RegisterScreenPreview() {
    val viewModel = hiltViewModel<RegisterViewModel>()
    RegisterScreen(rememberNavController(), viewModel)
}