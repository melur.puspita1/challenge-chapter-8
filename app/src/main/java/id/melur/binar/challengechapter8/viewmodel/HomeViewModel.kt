package id.melur.binar.challengechapter8.viewmodel

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import id.melur.binar.challengechapter8.datastore.LoginDataStore
import id.melur.binar.challengechapter8.model.movie.Resource
import id.melur.binar.challengechapter8.repository.MovieRepository
import id.melur.binar.challengechapter8.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val movieRepository: MovieRepository,
    private val loginDataStore: LoginDataStore
) : ViewModel() {

    var username: String? = ""

    fun clearDataStore() {
        viewModelScope.launch {
            loginDataStore.clearData()
        }
    }

    fun getEmail(): LiveData<String> {
        return loginDataStore.getEmail().asLiveData()
    }

    fun getUsername(email: String) {
        viewModelScope.launch {
            username = userRepository.getUsernameByEmail(email)
        }
    }

    fun getMovie() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepository.getMovie("dabe4a0a4b3b05a755e4b510103fce91")))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }
}