package id.melur.binar.challengechapter8.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.melur.binar.challengechapter8.model.user.User
import id.melur.binar.challengechapter8.repository.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val userRepository: UserRepository) :
    ViewModel() {

    var notregistered: Boolean = false

    var username by mutableStateOf("")
    fun usernameTextField(text: String) {
        username = text
    }

    var email by mutableStateOf("")
    fun emailTextField(text: String) {
        email = text
    }

    var password by mutableStateOf("")
    fun passwordTextField(text: String) {
        password = text
    }

    var confirm by mutableStateOf("")
    fun confirmTextField(text: String) {
        confirm = text
    }

    var passwordVisible by mutableStateOf(false)
    var confPasswordVisible by mutableStateOf(false)

    fun checkRegistered() {
        viewModelScope.launch {
            val result = userRepository.getUserByEmail(email)
            if (result == null) {
                notregistered = true
            }
        }
    }

    fun insertUser(user: User) {
        viewModelScope.launch {
            userRepository.insert(user)
        }
    }
}