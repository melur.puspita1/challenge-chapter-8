package id.melur.binar.challengechapter8.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.melur.binar.challengechapter8.datastore.LoginDataStore
import id.melur.binar.challengechapter8.repository.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val loginDataStore: LoginDataStore
) : ViewModel() {

    var notregistered: Boolean = false
    var login: Boolean = false

    var email by mutableStateOf("")
    fun emailTextField(text: String) {
        email = text
    }

    var password by mutableStateOf("")
    fun passwordTextField(text: String) {
        password = text
    }

    var passwordVisible by mutableStateOf(false)

    fun checkRegistered() {
        viewModelScope.launch {
            val result = userRepository.getUserByEmail(email)
            if (result == null) {
                notregistered = true
            }
        }
    }

    fun checkLogin(email: String) {
        viewModelScope.launch {
            val result = userRepository.getUserByEmail(email)
            if (result != null) {
                if (result.email == email && result.password == password) {
                    login = true
                }
            }
        }
    }

    fun saveDataStore(email: String) {
        viewModelScope.launch {
            loginDataStore.saveDataLogin(email)
        }
    }
}