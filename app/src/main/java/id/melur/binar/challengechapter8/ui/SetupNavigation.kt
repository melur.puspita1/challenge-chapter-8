package id.melur.binar.challengechapter8.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import id.melur.binar.challengechapter8.ui.screen.*
import id.melur.binar.challengechapter8.viewmodel.*

@Composable
fun SetupNavigation(
    registerViewModel: RegisterViewModel,
    loginViewModel: LoginViewModel,
    homeViewModel: HomeViewModel,
    splashViewModel: SplashViewModel,
    detailMovieViewModel: DetailMovieViewModel
) {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "splash") {
        composable("splash") {
            SplashScreen(
                navController = navController,
                splashViewModel = splashViewModel
            )
        }

        composable("login") {
            LoginScreen(
                navController = navController,
                loginViewModel = loginViewModel
            )
        }

        composable("register") {
            RegisterScreen(
                navController = navController,
                registerViewModel = registerViewModel
            )
        }

        composable("home") {
            HomeScreen(
                navController = navController,
                homeViewModel = homeViewModel
            )
        }

        composable(
            "detail/{id}",
            arguments = listOf(navArgument("id") { type = NavType.IntType })
        ) {
            val movieId = it.arguments?.getInt("id") ?: 0

            DetailMovieScreen(
                movieId,
                detailMovieViewModel = detailMovieViewModel
            )

        }
    }
}