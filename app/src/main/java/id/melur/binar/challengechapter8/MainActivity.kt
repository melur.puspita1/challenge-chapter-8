package id.melur.binar.challengechapter8

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import id.melur.binar.challengechapter8.ui.SetupNavigation
import id.melur.binar.challengechapter8.ui.theme.ChallengeChapter8Theme
import id.melur.binar.challengechapter8.viewmodel.*

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val registerViewModel: RegisterViewModel by viewModels()
    private val loginViewModel: LoginViewModel by viewModels()
    private val homeViewModel: HomeViewModel by viewModels()
    private val splashViewModel: SplashViewModel by viewModels()
    private val detailMovieViewModel: DetailMovieViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ChallengeChapter8Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    SetupNavigation(
                        registerViewModel = registerViewModel,
                        loginViewModel = loginViewModel,
                        homeViewModel = homeViewModel,
                        splashViewModel = splashViewModel,
                        detailMovieViewModel = detailMovieViewModel
                    )
                }
            }
        }
    }
}