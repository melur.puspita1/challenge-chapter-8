package id.melur.binar.challengechapter8.repository

import id.melur.binar.challengechapter8.service.ApiService

class MovieRepository(private val apiService: ApiService) {
    suspend fun getMovie(key: String) = apiService.getMovie(key)
    suspend fun getMovieById(movieId: Int, key: String) = apiService.getMovieById(movieId, key)
}