package id.melur.binar.challengechapter8.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import id.melur.binar.challengechapter8.datastore.LoginDataStore
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(private val loginDataStore: LoginDataStore) :
    ViewModel() {

    fun checkLogin(): LiveData<Boolean> {
        return loginDataStore.getLogin().asLiveData()
    }
}