package id.melur.binar.challengechapter8.ui.screen

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import com.skydoves.landscapist.glide.GlideImage
import id.melur.binar.challengechapter8.model.movie.Status
import id.melur.binar.challengechapter8.viewmodel.DetailMovieViewModel

@Composable
fun DetailMovieScreen(id: Int, detailMovieViewModel: DetailMovieViewModel) {

    val context = LocalContext.current
    val movie by detailMovieViewModel.getMovieById(id).observeAsState()

    when (movie?.status) {
        Status.SUCCESS -> {
            val moviedata = movie!!.data!!
            ConstraintLayout(modifier = Modifier.fillMaxSize()) {
                val (backdrop, poster, title, date, overview) = createRefs()

                Box(modifier = Modifier
                    .constrainAs(backdrop) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }) {
                    GlideImage(
                        imageModel = "https://www.themoviedb.org/t/p/w220_and_h330_face/" + moviedata.backdropPath,
                        modifier = Modifier
                            .height(250.dp)
                            .fillMaxWidth()
                    )
                }

                Card(shape = RoundedCornerShape(10.dp), modifier = Modifier
                    .width(150.dp)
                    .height(250.dp)
                    .constrainAs(poster) {
                        top.linkTo(backdrop.bottom, margin = (-80).dp)
                        start.linkTo(parent.start, margin = 16.dp)
                    }
                ) {
                    GlideImage(
                        imageModel = "https://www.themoviedb.org/t/p/w220_and_h330_face/" + moviedata.posterPath
                    )
                }

                Text(
                    text = moviedata.title,
                    fontSize = MaterialTheme.typography.h6.fontSize,
                    maxLines = 4,
                    textAlign = TextAlign.Justify,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .width(180.dp)
                        .constrainAs(title) {
                            top.linkTo(backdrop.bottom, margin = 4.dp)
                            start.linkTo(poster.end, margin = 15.dp)
                        }
                )

                Text(
                    text = moviedata.releaseDate.convertDate() ?: "01-01-1000",
                    fontWeight = FontWeight.Bold,
                    fontSize = 13.sp,
                    modifier = Modifier
                        .constrainAs(date) {
                            start.linkTo(poster.end, margin = 15.dp)
                            top.linkTo(title.bottom, margin = 4.dp)
                        }

                )

                Text(
                    text = moviedata.overview,
                    maxLines = 15,
                    textAlign = TextAlign.Justify,
                    fontSize = 18.sp,
                    modifier = Modifier
                        .constrainAs(overview) {
                            top.linkTo(poster.bottom, margin = 12.dp)
                        }
                        .padding(start = 12.dp, end = 12.dp)
                )


            }

        }
        Status.ERROR -> {
            Toast.makeText(context, movie?.message, Toast.LENGTH_SHORT).show()
        }
        else -> {}
    }
}

@Preview(showBackground = true)
@Composable
fun DetailScreenPreview() {
    val viewModel = hiltViewModel<DetailMovieViewModel>()
    DetailMovieScreen(0, viewModel)
}