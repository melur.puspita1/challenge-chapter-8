package id.melur.binar.challengechapter8.ui.screen

import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.skydoves.landscapist.glide.GlideImage
import id.melur.binar.challengechapter8.model.movie.Status
import id.melur.binar.challengechapter8.viewmodel.HomeViewModel
import java.text.SimpleDateFormat
import java.util.*

@Preview(name = "Android greeting")
@Composable
fun HomeScreen(navController: NavHostController, homeViewModel: HomeViewModel) {

    val context = LocalContext.current

    val email: String by homeViewModel.getEmail().observeAsState("")
    homeViewModel.getUsername(email)

    val movie by homeViewModel.getMovie().observeAsState()

    Surface(color = Color.White) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (txtUsername, txtHome, txtLogout, rvData) = createRefs()

            Text(
                text = "Welcome, " + homeViewModel.username,
                fontSize = 16.sp,
                modifier = Modifier
                    .constrainAs(txtUsername) {
                        top.linkTo(parent.top, margin = 32.dp)
                        start.linkTo(parent.start, margin = 20.dp)
                    }
            )

            Text(
                text = "Logout",
                fontSize = 16.sp,
                modifier = Modifier
                    .constrainAs(txtLogout) {
                        top.linkTo(parent.top, margin = 32.dp)
                        end.linkTo(parent.end, margin = 20.dp)
                    }
                    .clickable {
                        homeViewModel.clearDataStore()
                        navController.navigate("login") {
                            popUpTo("home") {
                                inclusive = true
                            }
                        }
                    }
            )

            Text(
                text = "Home",
                fontSize = MaterialTheme.typography.h5.fontSize,
                modifier = Modifier
                    .constrainAs(txtHome) {
                        top.linkTo(parent.top, margin = 88.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )

            when (movie?.status) {
                Status.SUCCESS -> {
                    LazyVerticalGrid(
                        columns = GridCells.Fixed(2),
                        modifier = Modifier
                            .constrainAs(rvData) {
                                top.linkTo(txtHome.bottom, margin = 8.dp)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .fillMaxSize(),
                        contentPadding = PaddingValues(
                            start = 12.dp,
                            top = 4.dp,
                            end = 12.dp,
                            bottom = 4.dp
                        ),
                        content = {
                            val moviedata = movie!!.data!!.results
                            items(moviedata.size) { index ->
                                Column(modifier = Modifier
                                    .fillMaxSize()
                                    .clickable { navController.navigate("detail/${moviedata[index].id}") }
                                    .padding(start = 8.dp, top = 8.dp)) {

                                    Card(
                                        modifier = Modifier.fillMaxSize(),
                                        shape = RoundedCornerShape(4.dp)
                                    ) {
                                        GlideImage(
                                            imageModel = "https://www.themoviedb.org/t/p/w220_and_h330_face/" + moviedata[index].posterPath,
                                            contentScale = ContentScale.Crop,
                                            modifier = Modifier
                                                .width(400.dp)
                                                .height(300.dp)
                                        )
                                    }

                                    Text(
                                        text = moviedata[index].title, modifier = Modifier
                                            .padding(start = 4.dp)
                                    )

                                    moviedata[index].releaseDate.convertDate()
                                        ?.let {
                                            Text(
                                                text = it, modifier = Modifier
                                                    .padding(start = 4.dp)
                                            )
                                        }

                                }
                            }
                        })
                }
                Status.ERROR -> {
                    Toast.makeText(context, movie?.message, Toast.LENGTH_SHORT).show()
                }
                else -> {}
            }

        }
    }
}

fun String.convertDate(): String? {
    if (this.isEmpty()) {
        return "-"
    }
    val inputPattern = "yyyy-MM-dd"

    val outputPattern = "MMM dd, yyyy"

    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
    val outputFormat = SimpleDateFormat(outputPattern, Locale("in"))

    val inputDate = inputFormat.parse(this)

    return inputDate?.let {
        outputFormat.format(it)
    }
}


@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    val viewModel = hiltViewModel<HomeViewModel>()
    HomeScreen(navController = rememberNavController(), viewModel)
}