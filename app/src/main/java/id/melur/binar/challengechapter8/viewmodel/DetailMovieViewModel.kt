package id.melur.binar.challengechapter8.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import id.melur.binar.challengechapter8.model.movie.Resource
import id.melur.binar.challengechapter8.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    fun getMovieById(movieId: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(
                Resource.success(
                    movieRepository.getMovieById(
                        movieId,
                        "dabe4a0a4b3b05a755e4b510103fce91"
                    )
                )
            )
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }
}