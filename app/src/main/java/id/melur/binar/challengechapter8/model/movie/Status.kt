package id.melur.binar.challengechapter8.model.movie

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}