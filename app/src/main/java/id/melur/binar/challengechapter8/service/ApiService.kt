package id.melur.binar.challengechapter8.service

import id.melur.binar.challengechapter8.model.movie.response.MoviePopular
import id.melur.binar.challengechapter8.model.movie.response.MoviePopularItem
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/popular")
    suspend fun getMovie(@Query("api_key") key: String): MoviePopular

    @GET("movie/{movie_id}")
    suspend fun getMovieById(
        @Path("movie_id") movieId: Int,
        @Query("api_key") key: String
    ): MoviePopularItem
}